#!/usr/bin/python3

import os
import base64

def create_kafka_secret():
    kafka_api_key = os.environ.get('KAFKA_API_KEY')
    kafka_api_secret = os.environ.get('KAFKA_API_SECRET')

    if kafka_api_key is None or kafka_api_secret is None:
        print("Error: KAFKA_API_KEY or KAFKA_API_SECRET environment variables not set.")
        return

    encoded_secret = f"{kafka_api_key}:{kafka_api_secret}"
    secret = base64.b64encode(encoded_secret.encode()).decode()

    print(secret)

if __name__ == "__main__":
    create_kafka_secret()

import base64

# String to encode
text = "ronald"

# Encode to Base64
encoded_text = base64.b64encode(text.encode('utf-8'))

# Print the encoded value
print(encoded_text.decode('utf-8'))
