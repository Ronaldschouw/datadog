#!/usr/bin/python3

from flask import Flask, jsonify
from prometheus_client import CollectorRegistry, Gauge, generate_latest
import base64
import os
import requests
import threading
import time


app = Flask(__name__)

refresh_interval = 60
kafka_api_key = os.environ.get('KAFKA_API_KEY')
kafka_api_secret = os.environ.get('KAFKA_API_SECRET')
encoded_secret = f"{kafka_api_key}:{kafka_api_secret}"
secret = base64.b64encode(encoded_secret.encode()).decode()
environment = os.environ.get('ENV')
headers = {'Authorization': f'Basic {secret}'}
confluent_cluster = ''
confluent_endpoint = ''
link_name = ''
url = ''
if environment == 'accept':
    confluent_cluster = "lkc-x70xw1"
    confluent_endpoint = f'{confluent_cluster}-pldo48'
    link_name="default-accept-cluster-link"
    url = f'https://{confluent_endpoint}.westeurope.azure.glb.confluent.cloud/kafka/v3/clusters/{confluent_cluster}/links/{link_name}/mirrors'
elif environment == 'prod':
    confluent_cluster = "lkc-7q7nk1"
    confluent_endpoint = f'{confluent_cluster}-gjw5yn'
    link_name="default-prod-cluster-link"
    url = f'https://{confluent_endpoint}.westeurope.azure.glb.confluent.cloud/kafka/v3/clusters/{confluent_cluster}/links/{link_name}/mirrors'
else:
    print("Environment variable ENV is not set to 'prod' or 'accept'")
    exit()

# Create a Prometheus metric
registry = CollectorRegistry()
gauge = Gauge('cluster_link_mirror_topic_status',
              'The status of the mirror topic',
              labelnames=['env', 'resource_id', 'integration', 'resource_type', 'team', 'link_name', 'topic'],
              registry=registry)

def get_data():
    response = requests.get(url, headers=headers)
    # Check if the request was successful
    if response.status_code == 200:
        # Parse the JSON response
        return response.json()
    else:
        print(f"Request failed with status code {response.status_code}")
        return None

def write_to_file(data):
        # Extract and print the mirror_topic_name
        for mirror in data['data']:
            # Check the mirror_status and print the mirror_topic_name with the corresponding value
            if mirror['mirror_status'] == "ACTIVE":
                value = 0.0
            elif mirror['mirror_status'] == "FAILED":
                value = 1.0
            elif mirror['mirror_status'] == "LINK_FAILED":
                value = 2.0
            elif mirror['mirror_status'] == "LINK_PAUSED":
                value = 3.0
            elif mirror['mirror_status'] == "PAUSED":
                value = 4.0
            elif mirror['mirror_status'] == "PENDING_STOPPED":
                value = 5.0
            elif mirror['mirror_status'] == "SOURCE_UNAVAILABLE":
                value = 6.0
            elif mirror['mirror_status'] == "STOPPED":
                value = 7.0
            elif mirror['mirror_status'] == "PENDING_MIRROR":
                value = 8.0
            elif mirror['mirror_status'] == "PENDING_SYNCHRONIZE":
                value = 9.0
            else:
                value = -1.0  # Indicating an unknown status
           
            gauge.labels(env=environment, resource_id=confluent_cluster, integration="confluent", resource_type="kafka", team="reliability-engineering", link_name=link_name, topic=mirror['mirror_topic_name']).set(value)
           
            # Output metric to a file
            with open('/work-dir/metrics.txt', 'w') as file:
                file.write(generate_latest(registry).decode())

def update_metrics():
    while True:
        data = get_data()
        if data:
            write_to_file(data)
        time.sleep(refresh_interval)

# Start the background task to update metrics
update_thread = threading.Thread(target=update_metrics)
update_thread.daemon = True
update_thread.start()

@app.route('/metrics', methods=['GET'])
def metrics():
    with open('/work-dir/metrics.txt', 'r') as file:
        metrics_content = file.read()
    return metrics_content, 200, {'Content-Type': 'text/plain; charset=utf-8'}


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8075)
