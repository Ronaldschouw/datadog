FROM python:3.12-alpine as build

WORKDIR /app

COPY mirror_info_flask.py /app
COPY requirements.txt /app

RUN apk add binutils
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir pyinstaller

RUN pyinstaller --onefile mirror_info_flask.py

##############################################################

FROM alpine
COPY --from=build /app/dist/mirror_info_flask /usr/local/bin/mirror_info_flask
ENV PYTHONUNBUFFERED=0

RUN mkdir /work-dir

CMD [ "mirror_info_flask"]
