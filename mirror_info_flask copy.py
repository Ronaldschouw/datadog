#!/usr/bin/python3

from flask import Flask, jsonify
import requests
import os
import threading
import time

app = Flask(__name__)

refresh_interval = 60
secret = os.environ.get('CREDS')
environment = os.environ.get('ENV')
headers = {'Authorization': f'Basic {secret}'}
url = ''
if environment == 'accept':
    url = 'https://lkc-x70xw1-pldo48.westeurope.azure.glb.confluent.cloud/kafka/v3/clusters/lkc-x70xw1/links/default-accept-cluster-link/mirrors'
    #insert = "cluster_link_mirror_topic_status{env=accept,resource_id=lkc-x70xw1,integration=confluent,resource_type=kafka,team=reliability-engineering,link_name=default-accept-cluster-link,"
    insert = "cluster_link_mirror_topic_status{"
elif environment == 'prod':
    url = 'https://lkc-7q7nk1-gjw5yn.westeurope.azure.glb.confluent.cloud/kafka/v3/clusters/lkc-7q7nk1/links/default-prod-cluster-link/mirrors'
    insert = "cluster_link_mirror_topic_status{env=prod,resource_id=lkc-7q7nk1,integration=confluent,resource_type=kafka,team=reliability-engineering,link_name=default-prod-cluster-link,"
else:
    print("Environment variable ENV is not set to 'prod' or 'accept'")
    exit()
haakje = "}"

def get_data():
    response = requests.get(url, headers=headers)
    # Check if the request was successful
    if response.status_code == 200:
        # Parse the JSON response
        return response.json()
    else:
        print(f"Request failed with status code {response.status_code}")
        return None

def write_to_file(data):
    with open('/work-dir/metrics.txt', 'w') as file:
        # Extract and print the mirror_topic_name
        for mirror in data['data']:
            # Check the mirror_status and print the mirror_topic_name with the corresponding value
            if mirror['mirror_status'] == "ACTIVE":
                value = 0.0
            elif mirror['mirror_status'] == "FAILED":
                value = 1.0
            elif mirror['mirror_status'] == "LINK_FAILED":
                value = 2.0
            elif mirror['mirror_status'] == "LINK_PAUSED":
                value = 3.0
            elif mirror['mirror_status'] == "PAUSED":
                value = 4.0
            elif mirror['mirror_status'] == "PENDING_STOPPED":
                value = 5.0
            elif mirror['mirror_status'] == "SOURCE_UNAVAILABLE":
                value = 6.0
            elif mirror['mirror_status'] == "STOPPED":
                value = 7.0
            elif mirror['mirror_status'] == "PENDING_MIRROR":
                value = 8.0
            elif mirror['mirror_status'] == "PENDING_SYNCHRONIZE":
                value = 9.0
            else:
                value = -1.0  # Indicating an unknown status
    
            # write to a metrics.txt file
            file.write(f"{insert}topic=\"{mirror['mirror_topic_name']}\"{haakje} {value}\n")

def update_metrics():
    while True:
        data = get_data()
        if data:
            write_to_file(data)
        time.sleep(refresh_interval)

# Start the background task to update metrics
update_thread = threading.Thread(target=update_metrics)
update_thread.daemon = True
update_thread.start()

@app.route('/metrics', methods=['GET'])
def metrics():
    with open('/work-dir/metrics.txt', 'r') as file:
        metrics_content = file.read()
    return metrics_content, 200, {'Content-Type': 'text/plain; charset=utf-8'}


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8075)


